import { Component, OnInit} from '@angular/core';
import {FormControl, ReactiveFormsModule } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import {AppSerService , LeadModel} from '../app-ser.service';
import {from, Observable, iif , of, NEVER} from 'rxjs';
import {debounceTime, map, startWith, switchMap} from 'rxjs/operators';
import { Items} from '../interface';
import {
  MatSnackBar,
  MatSnackBarHorizontalPosition,
  MatSnackBarVerticalPosition,
} from '@angular/material/snack-bar';
@Component({
  selector: 'app-ocr',
  templateUrl: './ocr.component.html',
  styleUrls: ['./ocr.component.css']
})
export class OcrComponent implements OnInit {
  title = 'project';
  uploadedFileNames: any = [];
  imgURL: any;
  showUploadContainer: any = true;
  uploadMessage: any = '';
  selectedFile = null;
  resData: any;
  email = '';
  website = '';
  phone = '';
  Industry = '';
  listesCh = [];
  listesNb = [];
fullName = '';
isRight = false;
firstName = '';
companyName = '';
lastName = '';
isShown = false ;
isHidden = false;
cross = true;
destactive = true;
sf = true;
sfh = false;
text = 'Drag-n drop a file or click to browse here';
seachTextModel: string;
selectedFirstName = '';
selectedLastName  = '';
selectedCompanyName = '';
selectedphoneNumber = '';
selectedIndustry = '';
hide = true;
hide2 = true;
username: FormControl = new FormControl('');
password: FormControl = new FormControl('');
c1 = '' ;
c2 = '';
lead: LeadModel;
mes: any;
horizontalPosition: MatSnackBarHorizontalPosition = 'center';
verticalPosition: MatSnackBarVerticalPosition = 'top';
error = '';
searchText: FormControl = new FormControl('');
public results: Observable<any> ;
searchText2: FormControl = new FormControl('');
public results2: Observable<any> ;
searchText3: FormControl = new FormControl('');
public results3: Observable<any> ;

constructor(private http: HttpClient , private appService: AppSerService, private snackBar: MatSnackBar) {
  this.lead = new LeadModel(null, null, null, null, null, null, null);
}

// tslint:disable-next-line:use-lifecycle-interface
ngOnInit() {
  this.results = this.searchText.valueChanges.pipe(
    startWith(''),
    debounceTime(300),    // delay emits
    switchMap(value =>
      iif(
        () => !!value,
        this.lookup(value),
        NEVER
      )
    )
  );

  this.results2 = this.searchText2.valueChanges.pipe(
    startWith(''),
    debounceTime(300),    // delay emits
    switchMap(value =>
      iif(
        () => !!value,
        this.lookuplastname(value),
        NEVER
      )
    )
  );
  this.results3 = this.searchText3.valueChanges.pipe(
    startWith(''),
    debounceTime(300),    // delay emits
    switchMap(value =>
      iif(
        () => !!value,
        this.lookupCompany(value),
        NEVER
      )
    )
  );

  }

  lookup(value: string): Observable<any> {
    return this.appService.autoComplete(value.toLowerCase()).pipe(
      // map the item property of the github results as our return object
     /*  map(results => {
        console.log('data:', results.hits.hits);
        // results.hits.hits;
      }) */
      map(results => results.hits.hits)

    );
  }

  lookuplastname(value: string): Observable<any> {
    return this.appService.autoCompleteLastname(value.toLowerCase()).pipe(
      // map the item property of the github results as our return object
     /*  map(results => {
        console.log('data:', results.hits.hits);
        // results.hits.hits;
      }) */
      map(results => results.hits.hits)

    );
  }

  lookupCompany(value: string): Observable<any> {
    return this.appService.autoCompleteCompanyname(value.toLowerCase()).pipe(
      // map the item property of the github results as our return object
     /*  map(results => {
        console.log('data:', results.hits.hits);
        // results.hits.hits;
      }) */
      map(results => results.hits.hits)

    );
  }

uploadFile(event) {
    if ( this.uploadedFileNames.length > 0) {
      this.uploadedFileNames = [];
    }

    /* add the first dragged file (draggin multiple files is allowed by browsers, we ignore them)*/
    if ( event.length > 0) {
      const file = event[0];
      this.uploadedFileNames.push(file.name);
      this.selectedFile = event[0];
      const mimeType = file.type;
      if (mimeType.match(/image\/*/) != null) {
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = (_event) => {
          this.imgURL = reader.result;


        };
        this.text = 'Your file ' + file.name + ' is ready to Scan it';
        this.uploadMessage = 'Great! The Card is ready to use.';
        this.cross = false;
        this.isShown = true;
        this.isHidden = true;
        console.log(this.uploadMessage);
        console.log(file.name);
      } else {
        this.uploadMessage = 'Unable to upload this file because it is not an image.';
        console.log(this.uploadMessage);

      }
    } else {
      this.uploadMessage = 'Upload problem !';
      console.log(this.uploadMessage);
    }
    this.updateUploadContainerVisibility();

  }
  updateUploadContainerVisibility() {
    this.showUploadContainer = true;
    this.uploadedFileNames = [];
  }
  deleteAttachment(index) {
    this.uploadedFileNames.splice(index, 1);
    this.text = 'Drag-n drop a file or click to browse here';
    this.selectedFile = null;
    this.isRight = false;
    this.isShown = ! this.isShown;
    this.cross = true;
    this.updateUploadContainerVisibility();
    this.isHidden = false;
    this.imgURL = null;
    this.reset();
    this.hide = true;
    this.hide2 = true;

  }

reset() {
  this.resData = null;
  this.selectedFirstName = '';
  this.selectedCompanyName = '';
  this.companyName = '';
  this.selectedLastName = '';
  this.selectedphoneNumber = '';
  this.c1 = '' ;
  this.c2 = '';
  this.firstName = '';
  this.companyName = '';
  this.lastName = '';
  this.email = '';
  this.website = '';
  this.phone = '';
}


  activateOCR() {
    console.log('hello');
    const payload = new FormData();
    console.log(this.selectedFile);
    payload.append('file', this.selectedFile, this.selectedFile.name);

    this.appService.imageConverter(payload).subscribe((data: any) => {
        this.resData = data;
        console.log(this.resData);
      });
    this.destactive = false;
  }
  printVal() {
    console.log(this.fullName);
    console.log(this.fullName.split(' '));
    if (this.fullName.split(' ').length > 1) {
       console.log('length > 1');
       this.isRight = true;
       this.firstName = this.fullName.split(' ')[0];
       this.lastName = this.fullName.split(' ')[1];
       if (this.firstName !== '' && this.lastName !== '') {
       this.selectedFirstName = this.firstName.toLowerCase();
       this.selectedLastName = this.lastName.toLowerCase();

      }
    } else {
    console.log('erreur de selection');
    this.isRight = false;
    this.firstName = '';
    this.lastName = '';
  }


    }



    saveName() {
      console.log('selectedFirstName :', this.selectedFirstName);
      console.log('firstname :', this.firstName);

      if (this.selectedFirstName !== this.firstName.toLowerCase()) {
      this.appService.addName(this.selectedFirstName).subscribe((data) => {
        console.log(data);
        alert('firstName saved');
      });
    }
      if (this.selectedLastName !== this.lastName.toLowerCase()) {
      this.appService.addLastname(this.selectedLastName).subscribe((data) => {
        console.log(data);
        alert('lastName saved');
      });
    }
      console.log('selectedcompanyname :', this.selectedCompanyName);
      console.log('companyname :', this.companyName);


      if (this.selectedCompanyName !== this.companyName.toLowerCase()) {
      this.appService.addCompanyName(this.selectedCompanyName).subscribe((data) => {
        console.log(data);
        alert('CompanyName saved');
      });
    }

    }

    printVal2() {
      console.log(this.companyName);
      this.selectedCompanyName = this.companyName.toLowerCase();

    }
    printVal3() {
      console.log(this.phone);
      this.selectedphoneNumber = this.phone;

    }
    printVal4() {
      console.log(this.Industry);
      this.selectedIndustry = this.Industry;

    }
    showit() {
      this.hide = !this.hide;
    }
    showit2() {
      this.hide2 = !this.hide2;
    }


openSnackBar(msg: string) {
 this.snackBar.open(msg, '', {
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
        duration: 2000,
        panelClass: ['warning']
      });
    }





submit(index) {
  console.log('clicked');
  this.saveName();
  this.lead.FirstName = this.selectedFirstName;
  this.lead.LastName = this.selectedLastName;
  this.lead.Company = this.selectedCompanyName;
  this.lead.Email = this.resData.email;
  this.lead.Website = this.resData.website;
  this.lead.Phone = this.selectedphoneNumber;
  this.lead.Industry = this.selectedIndustry;
  console.log(this.lead);
  this.appService.sendDatatosalesforce(this.lead).subscribe(value => {
    console.log(value);
    this.mes = value;

    console.log('message :', this.mes.response);
    if (this.mes.response === 'erreur authentification') {
      this.openSnackBar('erreur authentification');
      this.error = 'Username or password is incorrect';
    } else {
    this.openSnackBar('record saved successfully');
    this.deleteAttachment(index);
     }


  },
  error => {
      console.log(error);
  });



}


}
