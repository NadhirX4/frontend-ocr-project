import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { OcrComponent } from './ocr/ocr.component';

const routes: Routes = [
  {path: '', component: LoginComponent},
  {path:  'login', component: LoginComponent},
  {path:  'ocr', component: OcrComponent}


];


@NgModule({
  imports: [
    CommonModule, RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
